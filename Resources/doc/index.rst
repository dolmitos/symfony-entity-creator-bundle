DolmITEntityCreatorBundle
==================================

Installation
------------

Step 1: Download DolmITEntityCreatorBundle using composer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash
    $ composer require dolmitos/symfony-entity-creator-bundle

Step 2: Enable the bundle
~~~~~~~~~~~~~~~~~~~~~~~~~
Enable the bundle in the kernel::

    <?php
    // app/AppKernel.php

    public function registerBundles()
    {
        $bundles = array(
            // ...
            new DolmIT\EntityCreatorBundle\DolmITEntityCreatorBundle(),
            // ...
        );
    }
