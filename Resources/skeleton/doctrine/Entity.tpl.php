<?= "<?php\n" ?>

namespace <?= $namespace ?>;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits;

/**
* @ORM\Entity(repositoryClass="<?= $repository_full_class_name ?>")
*/
class <?= $class_name . " implements IEntity\n" ?>
{

    use Traits\Timestampable;
    use Traits\Blameable;
    use Traits\EntityStatus;
}