<?php
/*
 * This file is part of the DolmITEntityCreatorBundle package.
 *
 * (c) Dolm IT <https://dolmit.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DolmIT\EntityCreatorBundle;


use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DolmITEntityCreatorBundle
 */
class DolmITEntityCreatorBundle extends Bundle
{

}