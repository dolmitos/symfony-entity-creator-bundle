<?php
/**
 * Created by PhpStorm.
 * Date: 21.06.18
 * Time: 22:02
 */

namespace DolmIT\EntityCreatorBundle\Command;

use Symfony\Bundle\MakerBundle\FileManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractFileManagerCommand extends AbstractCommand
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var FileManager
     */
    protected $fileManager;

    /**
     * @var string
     */
    protected $rootPath;

    /**
     * Get command name
     *
     * @return string
     */
    abstract function getCommandName(): string;

    /**
     * CreateEntityCommand constructor.
     *
     * @param string|null $name
     * @param ContainerInterface $container
     */
    public function __construct(string $name = null, ContainerInterface $container)
    {
        parent::__construct($name);

        $this->container = $container;
        $this->fileManager = $this->container->get('maker.file_manager');
        $this->rootPath = $container->get('kernel')->getProjectDir() . DIRECTORY_SEPARATOR;
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName($this->getCommandName());
    }

    /**
     * Get file manager
     *
     * @return FileManager
     */
    public function getFileManager(): FileManager
    {
        return $this->fileManager;
    }

    /**
     * Get Root path
     *
     * @return string
     */
    public function getRootPath(): string
    {
        return $this->rootPath;
    }
}