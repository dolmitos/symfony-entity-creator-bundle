<?php
/**
 * Created by PhpStorm.
 * Date: 21.06.18
 * Time: 21:46
 */

namespace DolmIT\EntityCreatorBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use DolmIT\EntityCreatorBundle\Command\Creator\Entity\EntityCreator;

class CreateEntityCommand extends AbstractFileManagerCommand
{

    CONST COMMAND_NAME = 'create:entity';

    /**
     * CreateEntityCommand constructor.
     *
     * @param null|string $name
     * @param ContainerInterface $container
     */
    public function __construct(?string $name = null, ContainerInterface $container)
    {
        parent::__construct($name, $container);
    }

    /**
     * Get command name
     *
     * @return string
     */
    public function getCommandName(): string
    {
        return static::COMMAND_NAME;
    }

    /**
     * Execute command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

        $entityCreator = new EntityCreator($this);
        $entityCreator->askEntityName();
        $entityCreator->askAddNewField();
        $entityCreator->saveEntity();
    }
}