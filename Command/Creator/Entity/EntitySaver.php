<?php
/**
 * Created by PhpStorm.
 * Date: 30.06.18
 * Time: 18:05
 */

namespace DolmIT\EntityCreatorBundle\Command\Creator\Entity;

use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\Util\ClassNameDetails;
use DolmIT\EntityCreatorBundle\Command\AbstractFileManagerCommand;

class EntitySaver
{
    /**
     * @var EntityCreator
     */
    protected $entityCreator;

    public function __construct(EntityCreator $entityCreator)
    {
        $this->entityCreator = $entityCreator;
    }

    /**
     * Save entity
     */
    public function save()
    {
        $this->printEntityInfo();

        $this->createEntity();

        foreach($this->getEntity()->getFields() as $field) {
            if($field instanceof EntityField) {
                $this->printFieldInfo($field);
                $field->save();
            }
        }
    }

    /**
     * Print entity info
     */
    protected function printEntityInfo()
    {
        $this->getCommand()->write('Entity name: ' . $this->getEntity()->getName());
        $this->getCommand()->write('');
    }

    /**
     * Print field info
     * @param EntityField $field
     */
    protected function printFieldInfo(EntityField $field)
    {
        $this->getCommand()->write('Name: ' . $field->getName());
        $this->getCommand()->write('Type: ' . $field->getType());
        $this->getCommand()->write('Length: ' . $field->getlength());
        $this->getCommand()->write('');
    }

    /**
     * Create entity class
     */
    protected function createEntity()
    {
        $fileManager = $this->getCommand()->getFileManager();
        $namespacePrefix = 'App\\';
        $generator = new Generator($fileManager, $namespacePrefix);
        $bundleRootPath = dirname(__FILE__) . '/../../../';
        $skeletonPath = $bundleRootPath . 'Resources/skeleton/';
        $classDetails = $this->createEntityClass($generator, $skeletonPath);
        if($classDetails) {
            $generator->writeChanges();
        }
    }

    /**
     * Create entity class
     *
     * @param Generator $generator
     * @param string $skeletonPath
     * @return null|ClassNameDetails
     */
    public function createEntityClass(Generator $generator, string $skeletonPath): ?ClassNameDetails
    {
        $entityClassCreator = new EntityClassCreator($this->getEntityCreator());
        return $entityClassCreator->create($generator, $skeletonPath);
    }

    /**
     * Get entity creator
     * @return EntityCreator
     */
    protected function getEntityCreator():EntityCreator
    {
        return $this->entityCreator;
    }

    /**
     * Get command
     *
     * @return AbstractFileManagerCommand
     */
    public function getCommand(): AbstractFileManagerCommand
    {
        return $this->getEntityCreator()->getCommand();
    }

    /**
     * Get entity
     * @return Entity
     */
    protected function getEntity():Entity
    {
        return $this->getEntityCreator()->getEntity();
    }
}