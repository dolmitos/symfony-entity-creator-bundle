<?php
/**
 * Created by PhpStorm.
 * Date: 21.06.18
 * Time: 22:34
 */

namespace DolmIT\EntityCreatorBundle\Command\Creator\Entity;


class EntityField
{

    CONST TYPE_STRING = 'string';
    CONST TYPE_INTEGER = 'integer';
    CONST TYPE_SMALLINT = 'smallint';
    CONST TYPE_BIGINT = 'bigint';
    CONST TYPE_BOOLEAN = 'boolean';
    CONST TYPE_DECIMAL = 'decimal';
    CONST TYPE_FLOAT = 'float';
    CONST TYPE_DATE = 'date';
    CONST TYPE_TIME = 'time';
    CONST TYPE_DATETIME = 'datetime';
    CONST TYPE_TEXT = 'text';
    CONST TYPE_JSON_ARRAY = 'json_array';

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var int
     */
    protected $length;

    /**
     * EntityField constructor.
     *
     * @param Entity $entity
     */
    public function __construct(Entity $entity)
    {
        $this->entity = $entity;
    }

    /**
     * Get entity
     *
     * @return Entity
     */
    public function getEntity(): Entity
    {
        return $this->entity;
    }

    /**
     * Get available entity field types
     *
     * @return array
     */
    public static function getFieldTypes(): array
    {
        $types = [
            static::TYPE_STRING,
            static::TYPE_INTEGER,
            static::TYPE_SMALLINT,
            static::TYPE_BIGINT,
            static::TYPE_BOOLEAN,
            static::TYPE_DECIMAL,
            static::TYPE_FLOAT,
            static::TYPE_DATE,
            static::TYPE_TIME,
            static::TYPE_DATETIME,
            static::TYPE_TEXT,
            static::TYPE_JSON_ARRAY
        ];

        $return = [];
        foreach($types as $type) {
            $return[$type] = $type;
        }

        return $return;
    }

    /**
     * Save entity field
     */
    public function save()
    {
        $creator = $this->getEntity()->getCreator();
        $command = $creator->getCommand();
        $fileManager = $command->getFileManager();
        $entityPath = $this->getEntity()->getCreator()->getEntityPath();
        $manipulator = $creator->createClassManipulator($entityPath);
        $manipulator->addEntityField($this->getName(), $this->getAnnotationOptions());
        $fileManager->dumpFile($entityPath, $manipulator->getSourceCode());
    }

    /**
     * Get annotation options
     *
     * @return array
     */
    protected function getAnnotationOptions(): array
    {
        $annotationOptions = [
            'type' => $this->getType(),
            'nullable' => true
        ];

        return $annotationOptions;
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return null|string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return null|int
     */
    public function getLength(): ?int
    {
        return $this->length;
    }

    /**
     * @param int $length
     */
    public function setLength(int $length): void
    {
        $this->length = $length;
    }

}