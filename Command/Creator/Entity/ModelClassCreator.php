<?php
/**
 * Created by PhpStorm.
 * Date: 30.06.18
 * Time: 18:50
 */

namespace DolmIT\EntityCreatorBundle\Command\Creator\Entity;

use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\Util\ClassNameDetails;

class ModelClassCreator
{
    /**
     * @var EntityCreator
     */
    protected $entityCreator;

    public function __construct(EntityCreator $entityCreator)
    {
        $this->entityCreator = $entityCreator;
    }

    public function create(Generator $generator, string $skeletonPath, ClassNameDetails $entityClassDetails): ClassNameDetails
    {
        $entityName = $this->getEntity()->getName();
        $modelClassDetails = $generator->createClassNameDetails(
            $entityName,
            'Model\\',
            'Model'
        );

        $generator->generateClass(
            $modelClassDetails->getFullName(),
            $skeletonPath . 'doctrine/Model.tpl.php',
            [
                'entity_full_class_name' => $entityClassDetails->getFullName(),
                'entity_class_name' => $entityClassDetails->getShortName(),
            ]
        );

        return $modelClassDetails;
    }

    /**
     * Get entity creator
     * @return EntityCreator
     */
    protected function getEntityCreator():EntityCreator
    {
        return $this->entityCreator;
    }

    /**
     * Get entity
     * @return Entity
     */
    protected function getEntity():Entity
    {
        return $this->getEntityCreator()->getEntity();
    }

}