<?php
/**
 * Created by PhpStorm.
 * Date: 30.06.18
 * Time: 17:36
 */

namespace DolmIT\EntityCreatorBundle\Command\Creator\Entity;

use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\Util\ClassNameDetails;

class RepositoryClassCreator
{
    /**
     * @var EntityCreator
     */
    protected $entityCreator;

    public function __construct(EntityCreator $entityCreator)
    {
        $this->entityCreator = $entityCreator;
    }

    /**
     * Create repository class
     * @param Generator $generator
     * @param string $skeletonPath
     * @param ClassNameDetails $entityClassDetails
     * @return ClassNameDetails
     */
    public function create(Generator $generator, string $skeletonPath, ClassNameDetails $entityClassDetails): ClassNameDetails
    {
        $entityName = $this->getEntity()->getName();
        $repositoryClassDetails = $generator->createClassNameDetails(
            $entityName,
            'Repository\\',
            'Repository'
        );

        $entityAlias = strtolower($entityClassDetails->getShortName()[0]);
        $generator->generateClass(
            $repositoryClassDetails->getFullName(),
            $skeletonPath . 'doctrine/Repository.tpl.php',
            [
                'entity_full_class_name' => $entityClassDetails->getFullName(),
                'entity_class_name' => $entityClassDetails->getShortName(),
                'entity_alias' => $entityAlias,
            ]
        );

        return $repositoryClassDetails;
    }

    /**
     * Get entity creator
     * @return EntityCreator
     */
    protected function getEntityCreator():EntityCreator
    {
        return $this->entityCreator;
    }

    /**
     * Get entity
     * @return Entity
     */
    protected function getEntity():Entity
    {
        return $this->getEntityCreator()->getEntity();
    }
}