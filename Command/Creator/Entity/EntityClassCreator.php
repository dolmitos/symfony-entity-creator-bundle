<?php
/**
 * Created by PhpStorm.
 * Date: 30.06.18
 * Time: 17:59
 */

namespace DolmIT\EntityCreatorBundle\Command\Creator\Entity;

use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\Util\ClassNameDetails;

class EntityClassCreator
{
    /**
     * @var EntityCreator
     */
    protected $entityCreator;

    public function __construct(EntityCreator $entityCreator)
    {
        $this->entityCreator = $entityCreator;
    }

    /**
     * Create entity class
     * @param Generator $generator
     * @param string $skeletonPath
     * @return null|ClassNameDetails
     */
    public function create(Generator $generator, string $skeletonPath): ?ClassNameDetails
    {
        $entityName = $this->getEntity()->getName();
        $entityClassDetails = $generator->createClassNameDetails(
            $entityName,
            'Entity\\'
        );

        $classExists = class_exists($entityClassDetails->getFullName());
        if ($classExists) {
            return null;
        }

        $modelClassDetails = $this->createModelClass($generator, $skeletonPath, $entityClassDetails);
        $repositoryClassDetails = $this->createRepositoryClass($generator, $skeletonPath, $modelClassDetails);

        $generator->generateClass(
            $entityClassDetails->getFullName(),
            $skeletonPath . 'doctrine/Entity.tpl.php',
            [
                'repository_full_class_name' => $repositoryClassDetails->getFullName(),
            ]
        );

        $this->createDoctrineFile($modelClassDetails);

        return $entityClassDetails;
    }

    /**
     * Get entity creator
     * @return EntityCreator
     */
    protected function getEntityCreator():EntityCreator
    {
        return $this->entityCreator;
    }

    /**
     * Get entity
     * @return Entity
     */
    protected function getEntity():Entity
    {
        return $this->getEntityCreator()->getEntity();
    }

    /**
     * Create repository class
     * @param Generator $generator
     * @param string $skeletonPath
     * @param ClassNameDetails $entityClassDetails
     * @return ClassNameDetails
     */
    protected function createRepositoryClass(Generator $generator, string $skeletonPath, ClassNameDetails $entityClassDetails): ClassNameDetails
    {
        $repositoryClassCreator = new RepositoryClassCreator($this->getEntityCreator());
        return $repositoryClassCreator->create($generator, $skeletonPath, $entityClassDetails);
    }

    /**
     * Create doctrine file
     * @param ClassNameDetails $modelClassDetails
     */
    protected function createDoctrineFile(ClassNameDetails $modelClassDetails)
    {
        $doctrineFileCreator = new DoctrineFileCreator($this->getEntityCreator());
        $doctrineFileCreator->create($modelClassDetails);
    }

    protected function createModelClass(Generator $generator, string $skeletonPath, ClassNameDetails $entityClassDetails): ClassNameDetails
    {
        $modelClassCreator = new ModelClassCreator($this->getEntityCreator());
        return $modelClassCreator->create($generator, $skeletonPath, $entityClassDetails);
    }
}