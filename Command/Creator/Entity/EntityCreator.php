<?php
/**
 * Created by PhpStorm.
 * Date: 21.06.18
 * Time: 22:14
 */

namespace DolmIT\EntityCreatorBundle\Command\Creator\Entity;

use DolmIT\EntityCreatorBundle\Command\AbstractFileManagerCommand;
use Symfony\Bundle\MakerBundle\Util\ClassSourceManipulator;

class EntityCreator
{
    CONST QUESTION_ENTITY_NAME = 'What is the name of the entity? (Example: User)';
    CONST QUESTION_NEW_FIELD = 'Add new or edit field? (y/n)';
    CONST QUESTION_FIELD_NAME = 'Enter field name (Example: username)';
    CONST QUESTION_FIELD_TYPE = 'Select field type';
    CONST QUESTION_FIELD_LENGTH = 'Enter field length';

    CONST CHOICE_YES = 'y';
    CONST CHOICE_NO = 'n';

    CONST DEFAULT_YES_NO = self::CHOICE_YES;
    CONST DEFAULT_FIELD_TYPE = EntityField::TYPE_STRING;
    CONST DEFAULT_LENGTH = 11;
    CONST DEFAULT_ID_LENGTH = 11;

    /**
     * @var AbstractFileManagerCommand
     */
    protected $command;

    /**
     * @var Entity
     */
    protected $entity;

    /**
     * EntityCreator constructor.
     *
     * @param AbstractFileManagerCommand $command
     */
    public function __construct(AbstractFileManagerCommand $command)
    {
        $this->command = $command;
    }

    /**
     * Ask entity name
     */
    public function askEntityName()
    {
        $question = static::QUESTION_ENTITY_NAME;
        $name = $this->getCommand()->ask($question);
        $entityName = ucfirst($name);
        $entity = $this->findEntity($entityName);
        if ($entity instanceof Entity === false) {
            $entity = new Entity($this);
            $entity->setName($entityName);
            $idField = $entity->createNewField('id');
            $idField->setType('int');
            $idField->setLength(static::DEFAULT_ID_LENGTH);
        }
        $this->setEntity($entity);
    }

    /**
     * Get command
     *
     * @return AbstractFileManagerCommand
     */
    public function getCommand(): AbstractFileManagerCommand
    {
        return $this->command;
    }

    /**
     * Find entity by name
     *
     * @param string $name
     * @return Entity|null
     */
    protected function findEntity(string $name): ?Entity
    {
        $entityPath = $this->getEntityPath($name);
        $fileManager = $this->getCommand()->getFileManager();
        $exists = $fileManager->fileExists($entityPath);
        if($exists) {
            return $this->loadEntity($entityPath);
        }

        return null;
    }

    /**
     * Get entity path
     *
     * @param string|null $name
     * @return string
     */
    public function getEntityPath(string $name = null): string
    {
        if($name === null) {
            $name = $this->getEntity()->getName();
        }
        $rootPath = $this->getCommand()->getRootPath();
        $entityPath = $rootPath . 'src/Entity/' . $name . '.php';

        return $entityPath;
    }

    /**
     * @return Entity
     */
    public function getEntity(): Entity
    {
        return $this->entity;
    }

    /**
     * @param Entity $entity
     */
    protected function setEntity(Entity $entity): void
    {
        $this->entity = $entity;
    }

    /**
     * Load entity
     *
     * @param string $path
     * @return Entity
     */
    protected function loadEntity(string $path): Entity
    {

    }

    /**
     * Create class manipulator
     *
     * @param string $path
     * @return ClassSourceManipulator
     */
    public function createClassManipulator(string $path): ClassSourceManipulator
    {
        $sourceCode = $this->getCommand()->getFileManager()->getFileContents($path);
        $manipulator = new ClassSourceManipulator($sourceCode, true, false);
        $manipulator->setIo($this->getCommand()->getIo());

        return $manipulator;
    }

    /**
     * Ask add new field
     */
    public function askAddNewField()
    {
        $question = static::QUESTION_NEW_FIELD;
        $choices = static::getYesNoChoices();
        $answer = $this->getCommand()->ask($question, $choices, static::DEFAULT_YES_NO);

        if ($answer === static::CHOICE_YES) {
            $this->askFieldName();
            $this->askFieldType();
            $this->askFieldLength();
            $this->askAddNewField();
        }
    }

    /**
     * Get yes/no choices
     *
     * @return array
     */
    public static function getYesNoChoices(): array
    {
        return [
            static::CHOICE_YES => static::CHOICE_YES,
            static::CHOICE_NO => static::CHOICE_NO
        ];
    }

    /**
     * Ask entity field name
     */
    protected function askFieldName()
    {
        $question = static::QUESTION_FIELD_NAME;
        $name = $this->getCommand()->ask($question);
        $fieldName = strtolower($name);
        $field = $this->getEntity()->findField($fieldName);
        if($field instanceof EntityField !== true) {
            $field = $this->getEntity()->createNewField($fieldName);
        }
        $this->getEntity()->setCurrentField($field);
    }

    /**
     * Ask entity field type
     */
    protected function askFieldType()
    {
        $field = $this->getEntity()->getCurrentField();
        $oldType = $field->getType();
        if(empty($oldType)) {
            $oldType = static::DEFAULT_FIELD_TYPE;
        }
        $question = static::QUESTION_FIELD_TYPE;
        $availableTypes = EntityField::getFieldTypes();
        $type = $this->getCommand()->ask($question, $availableTypes, $oldType);
        $field->setType($type);
    }

    /**
     * Ask entity field length
     */
    protected function askFieldLength()
    {
        $field = $this->getEntity()->getCurrentField();
        $oldLength = $field->getLength();
        if(empty($oldLength)) {
            $oldLength = static::DEFAULT_LENGTH;
        }
        $question = static::QUESTION_FIELD_LENGTH;
        $length = $this->getCommand()->ask($question, [], $oldLength);
        $field->setLength($length);
    }

    /**
     * Save entity
     */
    public function saveEntity()
    {
        $entitySaver = new EntitySaver($this);
        $entitySaver->save();
    }
}