<?php
/**
 * Created by PhpStorm.
 * Date: 21.06.18
 * Time: 22:33
 */

namespace DolmIT\EntityCreatorBundle\Command\Creator\Entity;


class Entity
{

    /**
     * @var EntityCreator
     */
    protected $creator;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $fields;

    /**
     * @var EntityField
     */
    protected $currentField;

    /**
     * Entity constructor.
     * @param EntityCreator $creator
     */
    public function __construct(EntityCreator $creator)
    {
        $this->creator = $creator;
        $this->fields = [];
    }

    /**
     * Get entity creator
     *
     * @return EntityCreator
     */
    public function getCreator(): EntityCreator
    {
        return $this->creator;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set entity name
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return EntityField
     */
    public function getCurrentField(): EntityField
    {
        return $this->currentField;
    }

    /**
     * @param EntityField $currentField
     */
    public function setCurrentField(EntityField $currentField): void
    {
        $this->currentField = $currentField;
    }

    /**
     * Create new entity field
     *
     * @param string $name
     * @return EntityField
     */
    public function createNewField(string $name): EntityField
    {
        $field = new EntityField($this);
        $field->setName($name);
        $this->addField($field);

        return $field;
    }

    /**
     * Add new entity field
     *
     * @param EntityField $field
     */
    public function addField(EntityField $field)
    {
        $this->fields[] = $field;
    }

    /**
     * Find entity field by name
     *
     * @param $name
     * @return EntityField|null
     */
    public function findField($name): ?EntityField
    {
        $fields = $this->getFields();
        foreach($fields as $field) {
            if($field instanceof EntityField && $field->getName() === $name) {
                return $field;
            }
        }

        return null;
    }

    /**
     * Get all entity fields
     *
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }
}