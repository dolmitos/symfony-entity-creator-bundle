<?php
/**
 * Created by PhpStorm.
 * Date: 30.06.18
 * Time: 17:08
 */

namespace DolmIT\EntityCreatorBundle\Command\Creator\Entity;


use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Doctrine\ORM\Mapping\MappingException;
use Doctrine\ORM\Tools\Export\ClassMetadataExporter;
use Doctrine\ORM\Tools\Export\ExportException;
use Symfony\Bundle\MakerBundle\Util\ClassNameDetails;

class DoctrineFileCreator
{
    /**
     * @var EntityCreator
     */
    protected $entityCreator;

    /**
     * @var string
     */
    protected $mappingPath;

    /**
     * DoctrineFileCreator constructor.
     * @param EntityCreator $entityCreator
     */
    public function __construct(EntityCreator $entityCreator)
    {
        $this->entityCreator = $entityCreator;
        $this->setMappingPath();
    }

    /**
     * Create doctrine file
     * @param ClassNameDetails $modelClassDetails
     */
    public function create(ClassNameDetails $modelClassDetails)
    {
        $mappingPath = $this->getMappingPath();

        if(file_exists($mappingPath)) {
            throw new \RuntimeException(sprintf('Cannot generate entity when mapping "%s" already exists.', $mappingPath));
        }

        $class = $this->getClassMetaInfo($modelClassDetails);
        $metadataExporter = new ClassMetadataExporter();

        try {
            $exporter = $metadataExporter->getExporter('xml');
            $mappingCode = $exporter->exportClassMetadata($class);

            if (!is_dir(dirname($mappingPath))) {
                mkdir(dirname($mappingPath), 0777, true);
            }

            file_put_contents($mappingPath, $mappingCode);
        } catch (ExportException $e) {
        }
    }

    /**
     * @param ClassNameDetails $modelClassDetails
     * @return ClassMetadataInfo
     */
    protected function getClassMetaInfo(ClassNameDetails $modelClassDetails):ClassMetadataInfo
    {
        $entityName = $this->getEntity()->getName();
        $entityClassName = 'App\\Model\\' . $modelClassDetails->getShortName();
        $class = new ClassMetadataInfo($entityClassName);
        $class->customRepositoryClassName = 'App\\Repository\\' . $entityName . 'Repository';
        try {
            $class->mapField([
                'fieldName' => 'id',
                'type' => 'integer',
                'id' => true
            ]);
        } catch (MappingException $e) {
        }
        $class->setIdGeneratorType(ClassMetadataInfo::GENERATOR_TYPE_AUTO);

        foreach($this->getEntity()->getFields() as $field) {
            if ($field instanceof EntityField) {
                if($field->getName() === 'id') {
                    continue;
                }
                try {
                    $class->mapField([
                        'fieldName' => $field->getName(),
                        'type' => $field->getType()
                    ]);
                } catch (MappingException $e) {
                }
            }
        }

        return $class;
    }

    /**
     * Set mapping path
     */
    protected function setMappingPath()
    {
        $rootPath = $this->getEntityCreator()->getCommand()->getRootPath();
        $entityName = $this->getEntity()->getName();
        $this->mappingPath = $rootPath .'src/Resources/config/doctrine/' . $entityName . '.orm.xml';
    }

    /**
     * Get entity creator
     * @return EntityCreator
     */
    protected function getEntityCreator():EntityCreator
    {
        return $this->entityCreator;
    }

    /**
     * Get entity
     * @return Entity
     */
    protected function getEntity():Entity
    {
        return $this->getEntityCreator()->getEntity();
    }

    /**
     * Get mapping path
     * @return string
     */
    protected function getMappingPath():string
    {
        return $this->mappingPath;
    }
}