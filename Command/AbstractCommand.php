<?php
/**
 * Created by PhpStorm.
 * Date: 21.06.18
 * Time: 20:57
 */

namespace DolmIT\EntityCreatorBundle\Command;

use Symfony\Bundle\MakerBundle\ConsoleStyle;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractCommand extends Command
{
    /**
     * @var ConsoleStyle
     */
    private $io;

    /**
     * Ask what do you want to do
     *
     * @param array $choices
     * @return null|string
     */
    public function askWhatToDo(array $choices = [])
    {
        $question = 'What do you want to do?';

        return $this->ask($question, $choices);
    }

    /**
     * Asks a question.
     *
     * @param string $question
     * @param array $choices
     * @param null $default
     * @return null|string
     */
    public function ask(string $question, array $choices = [], $default = null): ?string
    {
        if (empty($choices)) {
            $answer = $this->getIo()->ask($question, $default);
        } else {
            $answer = $this->getIo()->choice($question, $choices, $default);
        }

        return $answer;
    }

    /**
     * @return ConsoleStyle
     */
    public function getIo(): ConsoleStyle
    {
        return $this->io;
    }

    /**
     * @param ConsoleStyle $io
     */
    protected function setIo(ConsoleStyle $io)
    {
        $this->io = $io;
    }

    /**
     * Call next command
     *
     * @param string $command
     * @param OutputInterface $output
     */
    public function call(string $command, OutputInterface $output)
    {
        try {
            $arguments = [
                'command' => $command,
            ];

            $input = new ArrayInput($arguments);

            $cmd = $this->getApplication()->find((string)$command);
            $cmd->run($input, $output);
        } catch (\Exception $e) {
            $this->write($e->getMessage());
        }
    }

    /**
     * Writes a message to the output.
     *
     * @param string $text
     * @param bool $newline
     */
    public function write(string $text, bool $newline = true)
    {
        $this->getIo()->write($text, $newline);
    }

    /**
     * Execute command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setIo(new ConsoleStyle($input, $output));
        $this->clearConsole();
    }

    /**
     * Clear console
     */
    protected function clearConsole()
    {
        $this->write("\033\143");
    }
}